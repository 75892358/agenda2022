﻿using _0._0.DataTransfer.Dto;
using _0._0.DataTransfer.DtoAdditional;
using _3._0.Business.Business.Usuario;
using Microsoft.AspNetCore.Mvc;
using System.Reflection.Metadata.Ecma335;

namespace _5._0.DataAccess
{

    [ApiController]
    [Route("[controller]")]
    public class ActividadesController : ControllerBase
    {
        [HttpPost]
        [Route("[action]")]
        public ActionResult<DtoMessage> Insert([FromForm] DtoActividades dto)
        {
            BusinessActividad businessActividad = new();//instanciacion de la clase BusinessUsuario

            DtoMessage dtoMessage = businessActividad.Insert(dto);

            return dtoMessage;


        }
        [HttpGet]
        [Route("[action]")]
        public ActionResult<List<DtoActividades>> getAll()
        {
            BusinessActividad businessActividad = new();

            List<DtoActividades> lista = businessActividad.getAll();

            return lista;
        }
    }
}
