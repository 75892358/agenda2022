﻿using _0._0.DataTransfer.Dto;

namespace _3._0.Business.Business.Usuario
{
    public partial class BusinessUsuario
    {
        private void ValidationInsert(DtoUsuario dto) 
        {
            if (_repoService.ExistsByDni(dto.Dni)) 
            {
                _mo.listMessage.Add("El servicio que se trata de registrar ya existe en el sistema.");
            }
        }
    }
}