﻿using _0._0.DataTransfer.Dto;
using _0._0.DataTransfer.DtoAdditional;
using _3._0.Business.Generic;


namespace _3._0.Business.Business.Usuario
{
    public partial class BusinessUsuario:BusinessGeneric
    {
        public DtoMessage Insert(DtoUsuario dto) {
            dto.idUsuario = Guid.NewGuid().ToString();
            dto.FechaRegistro = DateTime.Now;
            dto.FechaModificacion = DateTime.Now;

            ValidationInsert(dto);

            if (_mo.existsMessage())
            {
                return _mo;
            }

            _repoService.Insert(dto);

            _mo.listMessage.Add("Operación realizada correctamente.");
            _mo.success();

            return _mo;

        
        }
        public List<DtoUsuario> getAll()
        {
            return _repoService.getAll();


        }
    }
}
