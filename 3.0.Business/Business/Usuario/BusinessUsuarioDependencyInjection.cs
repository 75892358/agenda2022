﻿using _3._0.Business.Generic;
using _4._0.Repository.Repository;
using _5._0.DataAccess.Query;
using Microsoft.Extensions.DependencyInjection;

namespace _3._0.Business.Business.Usuario
{
    public partial class BusinessUsuario:BusinessGeneric
    {
        private RepositoryUsuario _repoService { get; set; }

        protected override void InitDi()
        {
            _services.AddTransient<RepositoryUsuario, QUsuario>();

            InitProvider();

            _repoService = _provider.GetService<RepositoryUsuario>();
        }
    }
}