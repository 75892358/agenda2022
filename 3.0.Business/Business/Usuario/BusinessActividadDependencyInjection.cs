﻿using _3._0.Business.Generic;
using _4._0.Repository.Repository;
using _5._0.DataAccess.Query;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3._0.Business.Business.Usuario
{ 
        public partial class BusinessActividad : BusinessGeneric
        {
            private RepositoryActividad _repoService { get; set; }

            protected override void InitDi()
            {
                _services.AddTransient<RepositoryActividad, QActividad>();
                InitProvider();

                _repoService = _provider.GetService<RepositoryActividad>();
            }
        }
    
}
