﻿
using _0._0.DataTransfer.DtoAdditional;
using _5._0.DataAccess.Entity;
using Microsoft.EntityFrameworkCore;
namespace _5._0.DataAccess.Conection
{
   
    public  class DataBaseContex:DbContext
    {
        public DataBaseContex() { 
         InitAutoMapper.Start();
        }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Actividad> Actividades { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(DtoAppSettings.ConnectionStringSqlServer);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().ToTable("tusuario");
            modelBuilder.Entity<Actividad>().ToTable("tactividades");

            base.OnModelCreating(modelBuilder);
        }
    }
}
