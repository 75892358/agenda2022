﻿using _0._0.DataTransfer.Dto;
using _5._0.DataAccess.Entity;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._0.DataAccess
{
    public class InitAutoMapper
    {
        public static bool _initMapper = true;
        public static IMapper mapper;

        public static void Start()
        {
            if (_initMapper)
            {
                var config = new MapperConfiguration(cfg =>
                {
                  
                    cfg.CreateMap<DtoUsuario,Usuario>().MaxDepth(7);
                    cfg.CreateMap<Usuario, DtoUsuario>().MaxDepth(7);

                    cfg.CreateMap<DtoActividades, Actividad>().MaxDepth(7);
                    cfg.CreateMap<Actividad, DtoActividades>().MaxDepth(7);
                });

                mapper = config.CreateMapper();

                _initMapper = false;
            }
        }
    
}

}
