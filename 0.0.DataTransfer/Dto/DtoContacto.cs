﻿
using _0._0.DataTransfer.Generic;
using System.ComponentModel.DataAnnotations;
namespace _0._0.DataTransfer.Dto
{
    public class DtoContacto:DtoGeneric
    {
        public string idContacto { get; set; }
        public string idUsuario { get; set; }
        [Required(ErrorMessage = "el campo \"NombreContac\"es requerido.")]
        public string NombreContacto { get; set; }
        [Required(ErrorMessage = "el campo \"NumContacto\"es requerido.")]
        public string NumeroContacto { get; set; }
        [Required(ErrorMessage = "el campo \"EmailContaco\"es requerido.")]
        public string EmailContacto { get; set; }
        [Required(ErrorMessage = "el campo \"NotasContac\"es requerido.")]
        public string NotasContacto { get; set; }

    }
}
