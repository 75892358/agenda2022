﻿using _0._0.DataTransfer.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4._0.Repository.Repository
{
    public  interface RepositoryUsuario
    {
        public int Insert(DtoUsuario dto);
        public List<DtoUsuario> getAll();
        public bool ExistsByDni(string Dni);

       
    }
}
