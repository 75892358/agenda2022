﻿
using _0._0.DataTransfer.Generic;
using System.ComponentModel.DataAnnotations;

namespace _0._0.DataTransfer.Dto
{
    public class DtoActividades:DtoGeneric
    {
        public string idActividad { get; set; }
        public string idUsuario { get; set; }
        [Required(ErrorMessage = "el campo \"Titulo\"es requerido.")]
        public string Titulo { get; set; }
        public DateTime FechaProgramada { get; set; }
        public DateTime HoraProgramada { get; set; }
        
    }
}
