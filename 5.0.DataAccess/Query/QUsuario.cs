﻿using _0._0.DataTransfer.Dto;
using _4._0.Repository.Repository;
using _5._0.DataAccess.Conection;
using _5._0.DataAccess.Entity;


namespace _5._0.DataAccess.Query
{
    public class QUsuario : RepositoryUsuario
    {
        public bool ExistsByDni(string Dni)
        {
            using DataBaseContex dbc = new();

            return dbc.Usuarios.Where(w => w.Dni.Replace(" ", "") == Dni.Replace(" ", "")).Any();
        }
       
        public int Insert(DtoUsuario dto)
        {

            using DataBaseContex dbc = new();

            //Usuario usuario = new Usuario();

            Usuario usuario =InitAutoMapper.mapper.Map<Usuario>(dto);
            dbc.Usuarios.Add(usuario);

            return dbc.SaveChanges();
        }
        //retornamos el listado
        public List<DtoUsuario> getAll()
        {
            using (DataBaseContex dbc = new DataBaseContex())
            {
                return InitAutoMapper.mapper.Map<List<DtoUsuario>>(dbc.Usuarios.OrderBy(ob => ob.Nombres).ToList());
            }
        }

        
    }
}
