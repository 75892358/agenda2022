﻿using _0._0.DataTransfer.DtoAdditional;
using Microsoft.Extensions.DependencyInjection;

namespace _3._0.Business.Generic
{
    public abstract class BusinessGeneric
    {
        protected DtoMessage _mo;
        protected ServiceCollection _services;
        protected ServiceProvider _provider;

        public BusinessGeneric()
        {
            _services = new();

            InitDi();

            _mo = new();
        }

        protected virtual void InitDi() { }

        protected void InitProvider()
        {
            _provider = _services.BuildServiceProvider();
        }
    }
}