﻿using _0._0.DataTransfer.Dto;
using _4._0.Repository.Repository;
using _5._0.DataAccess.Conection;
using _5._0.DataAccess.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._0.DataAccess.Query
{
    public class QActividad : RepositoryActividad
    {
      

        public int Insert(DtoActividades dto)
        {

            using DataBaseContex dbc = new();

            //Usuario usuario = new Usuario();

            Actividad actividad = InitAutoMapper.mapper.Map<Actividad>(dto);
            dbc.Actividades.Add(actividad);

            return dbc.SaveChanges();
        }
        //retornamos el listado
        public List<DtoActividades> getAll()
        {
            using (DataBaseContex dbc = new DataBaseContex())
            {
                return InitAutoMapper.mapper.Map<List<DtoActividades>>(dbc.Actividades.OrderBy(ob => ob.idUsuario).ToList());
            }
        }

       
    }
    }
