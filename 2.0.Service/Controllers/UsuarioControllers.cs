using _0._0.DataTransfer.Dto;
using _0._0.DataTransfer.DtoAdditional;
using _3._0.Business.Business.Usuario;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
namespace _2._0.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        [EnableCors("cors")]
        [HttpPost]
        [Route("[action]")]
        public ActionResult<DtoMessage> Insert([FromForm] DtoUsuario dto)
        {
            BusinessUsuario businessUsuario = new();//instanciacion de la clase BusinessUsuario
         
            DtoMessage dtoMessage = businessUsuario.Insert(dto);

            return dtoMessage;
        }
        [HttpGet]
        [Route("[action]")]
        public ActionResult<List<DtoUsuario>> getAll()
        {
         BusinessUsuario businessUsuario=new();

             List<DtoUsuario> lista= businessUsuario.getAll();

            return lista;
        }
    }
}


